[
    {{- $t_first := true -}}
    {{- range . -}}
    {{- $target := .Target -}}
    {{- $image := .Target -}}
    {{- if gt (len $image) 127 -}}
        {{- $image = $image | regexFind ".{124}$" | printf "...%v" -}}
    {{- end}}
    {{- range .Vulnerabilities -}}
    {{- if $t_first -}}
      {{- $t_first = false -}}
    {{- else -}}
      ,
    {{- end -}}
    
        {
            "NvdCvssVectorV3": "{{ (index .CVSS (sourceID "nvd")).V3Score }}"
        }
        {{- end -}}
      {{- end }}
]
        
