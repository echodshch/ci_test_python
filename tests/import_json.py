import json
import os

def read_json(file):
    """ getting json from the file """
    if file:
        if os.path.getsize(file) > 0:
            with open(file, 'r') as f:
                data = json.load(f)
            return data
        else:
            raise Exception('The input JSON file is empty')

def cvss_average (list_json):
    obj_count = len(list_json)
    cvss_sum = 0.0

    for x in list_json:
        cvss_sum = cvss_sum + float(x['NvdCvssVectorV3'])

    cvss_average = cvss_sum / obj_count
    return cvss_average

def Average(lst):
    return sum(lst) / len(lst)

def main():
    try:
        json_files = ['./json/trivy_results.json']
        for file in json_files:
            json_content = read_json(file)
            cvss_score = cvss_average(json_content)
            file_result = open('results.txt','w')
            if 0 <= cvss_score <= 7:
                data = "Test passed \n"
            else:
                data = "Test not passed \n"
            data += "Average score " + str(cvss_score)

        file_result.write(data) 
        file_result.close()
            
    except Exception as exception_message:
        print('[-] Something went wrong: %s' % exception_message)


if __name__ == "__main__":
    print('[+] Starting the main module ' + '=' * 60)
    main()



       
